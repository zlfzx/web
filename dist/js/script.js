// const nav = document.querySelector('#navbar');
var nav = document.getElementById('navbar'),
    navRespon = document.getElementById('nav-respon');

// nav scroll
window.onscroll = () => {
    if(window.scrollY <= 10) nav.className = 'nav nav-utama'; else nav.className = 'nav nav-utama scroll';
    if(window.scrollY > 500) nav.classList.add = 'scroll';
}

// responsive navbar
document.getElementById('nav-res').addEventListener('click', function(){
    document.getElementById('nav-respon').classList.toggle('active');
    document.getElementById('navbar').classList.toggle('nav-utama');
    if (navRespon.classList.contains('active')) {
        nav.style.backgroundColor = '#102027';
    } 
    else if (!navRespon.classList.contains('active')){
        nav.style.backgroundColor = '';
    }
});
